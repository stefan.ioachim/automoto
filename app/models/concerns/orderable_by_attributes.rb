# frozen_string_literal: true

module OrderableByAttributes
  extend ActiveSupport::Concern

  included do
    scope :by_recently_created, -> { order(created_at: :desc) }
  end
end
