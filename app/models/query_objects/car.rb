# frozen_string_literal: true

module QueryObjects
  # A class that implements the search functionality for the Car model
  class Car
    def initialize(scope = Car.all)
      @scope = scope
    end

    def self.multisearch(scope:, filters:)
      new(scope).multisearch(filters)
    end

    def multisearch(filters)
      safe_filters = filters || {}

      query(safe_filters)
      sort_and_order(safe_filters)
    end

    private

    def query(filters)
      query_filters = query_filters(filters)

      if query_filters.present?
        %i[brand model description]
          .select { |key| query_filters[key].present? }
          .each { |key| @scope = PgSearch.multisearch(query_filters[key]) }

        %i[doors seats body_type fuel_type drivetrain]
          .select { |key| query_filters[key].present? }
          .each { |key| @scope = @scope.where(key => query_filters[key]) }
      end

      @scope
    end

    def sort_and_order(filters)
      sorting = sorting_filters(filters)

      @scope.send("by_#{sorting}") if sorting.present?
      @scope.order(id: :asc) unless sorting.present?

      @scope
    end

    def query_filters(filters)
      new_filters = filters.dup
      new_filters.delete(:sorting)
      new_filters
    end

    def sorting_filters(filters)
      filters[:sorting]
    end
  end
end
