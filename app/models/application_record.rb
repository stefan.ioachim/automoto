# frozen_string_literal: true

# The top model that other models extend
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
