# frozen_string_literal: true

module Objects
  # Link class used for describing a filter link
  class Link
    attr_accessor :text, :url

    def initialize(text = nil, url = nil)
      @text = text
      @url = url
    end
  end
end
