# frozen_string_literal: true

module Objects
  # Class used for instantiating the sorting options for the sort drop-down
  class Sorting
    attr_reader :id, :text, :url

    def initialize(*args)
      id, text, url = args
      @id = id
      @text = text
      @url = url
    end
  end
end
