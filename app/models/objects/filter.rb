# frozen_string_literal: true

module Objects
  # Class used for instantiating filters
  class Filter
    attr_accessor :id, :text, :facets, :url
    attr_writer :active

    def initialize(*args, &block)
      id, text, url, value = args
      @id = id
      @url = url if url.present?
      @active = value.present? ? value : false
      @text = @active ? "#{text}: #{value}" : text
      @facets = Car.send(id).map(&block) if block.present?
    end

    def active
      @active || false
    end
  end
end
