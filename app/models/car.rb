# frozen_string_literal: true

# Car model
class Car < ApplicationRecord
  include PgSearch::Model
  include OrderableByAttributes

  belongs_to :user

  # validations
  validates_presence_of :brand, :model, :description

  # filters definitions
  multisearchable(
    against: %i[brand model description],
    additional_attributes: lambda { |car|
      {
        body_type: car.body_type,
        seats: car.seats,
        doors: car.doors,
        fuel_consumption_urban: car.fuel_consumption_urban,
        fuel_consumption_extra_urban: car.fuel_consumption_extra_urban,
        fuel_type: car.fuel_type,
        acceleration: car.acceleration,
        maximum_speed: car.maximum_speed,
        power: car.power,
        engine_displacement: car.engine_displacement,
        weight: car.weight,
        trunk_space_min: car.trunk_space_min,
        trunk_space_max: car.trunk_space_max,
        fuel_tank_capacity: car.fuel_tank_capacity,
        length: car.length,
        width: car.width,
        height: car.height,
        drivetrain: car.drivetrain
      }
    }
  )

  def name
    [brand, model].compact.join(' ')
  end

  class << self
    def multisearch(params)
      QueryObjects::Car.multisearch(filters: params, scope: Car.all)
    end

    def seats
      [2, 3, 4, 5, 6, 7, 8].freeze
    end

    def doors
      [2, 4, 6].freeze
    end

    def fuel_type
      %w[gasoline diesel electricity].freeze
    end

    def drivetrain
      ['all-wheel drive', 'four-wheel drive', 'front-wheel drive', 'rear-wheel drive'].freeze
    end

    def body_type
      Rails.cache.fetch('body_types', expires_in: 12.months) do
        distinct.pluck(:body_type).freeze
      end
    end
  end
end
