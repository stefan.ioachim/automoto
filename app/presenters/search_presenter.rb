# frozen_string_literal: true

# The presenter for search functionality
class SearchPresenter
  include Rails.application.routes.url_helpers

  def initialize(params)
    if params.blank? || params[:search].blank?
      @search_params = @search_params ? @search_params.except(:controller, :action, :page) : {}
    else
      @search_params = params.require(:search).permit(
        :brand, :model, :body_type, :seats, :doors,
        :fuel_consumption_urban, :fuel_consumption_extra_urban, :fuel_type, :acceleration, :maximum_speed,
        :power, :engine_displacement, :trunk_space_min, :trunk_space_max, :fuel_tank_capacity,
        :length, :width, :height, :drivetrain, :sorting
      )
    end
  end

  def filters
    results = []

    results << Objects::Filter.new(
      :clear,
      'Clear All',
      cars_path
    )

    results << Objects::Filter.new(
      :seats,
      'By seats',
      cars_path_without_filters(:seats),
      @search_params[:seats]
    ) { |x| Objects::Link.new("#{x} seats", cars_path_with_filters({ seats: x })) }

    results << Objects::Filter.new(
      :doors,
      'By doors',
      cars_path_without_filters(:doors),
      @search_params[:doors]
    ) { |x| Objects::Link.new("#{x} doors", cars_path_with_filters({ doors: x })) }

    results << Objects::Filter.new(
      :body_type,
      'By body type',
      cars_path_without_filters(:body_type),
      @search_params[:body_type]
    ) { |x| Objects::Link.new(x, cars_path_with_filters({ body_type: x })) }

    results << Objects::Filter.new(
      :fuel_type,
      'By fuel type',
      cars_path_without_filters(:fuel_type),
      @search_params[:fuel_type]
    ) { |x| Objects::Link.new(x, cars_path_with_filters({ fuel_type: x })) }

    results << Objects::Filter.new(
      :drivetrain,
      'By drivetrain',
      cars_path_without_filters(:drivetrain),
      @search_params[:drivetrain]
    ) { |x| Objects::Link.new(x, cars_path_with_filters({ drivetrain: x })) }

    results << Objects::Filter.new(
      :brand,
      'By brand',
      cars_path_without_filters(:brand),
      @search_params[:brand]
    )

    results << Objects::Filter.new(
      :model,
      'By model',
      cars_path_without_filters(:model),
      @search_params[:model]
    )

    results
  end

  def sortings
    results = []
    results << Objects::Sorting.new(nil, 'None')
    results << Objects::Sorting.new('recently_created', 'Newest first')
    results
  end

  def active_params_except(id)
    if @search_params
      @search_params.except(id)
    else
      empty_params
    end
  end

  private

  attr_reader :search_params

  def cars_path_with_filters(param)
    if @search_params.blank?
      cars_path(search: param)
    elsif param.is_a?(Hash)
      key = param.keys.first
      cars_path(search: @search_params.except(key).merge(param))
    else
      cars_path(search: @search_params.except(param))
    end
  end

  def cars_path_without_filters(param)
    if @search_params.blank?
      cars_path
    else
      cars_path(search: @search_params.except(param))
    end
  end

  def empty_params
    ActionController::Parameters.new.permit
  end
end
