# frozen_string_literal: true

# Defines the actions used for displaying , adding, deleting or updating a car
class CarsController < ApplicationController
  before_action :find_car, only: %i[show edit update destroy]

  def index
    page = params[:page] || 1
    @searched_cars = Car.multisearch(params[:search])
    @pagy, @cars = pagy(@searched_cars, page: page)
  end

  def show; end

  def new
    @car = Car.new
  end

  def create
    @car = current_user.cars.build(car_params)
    if @car.save
      redirect_to @car
    else
      render 'new'
    end
  end

  def edit
    authorize! :update, @car
  end

  def update
    if @car.update(car_params)
      redirect_to @car
    else
      render 'edit'
    end
  end

  def destroy; end

  private

  def find_car
    @car = Car.find(params[:id])
  end

  def car_params
    params.require(:car).permit(
      :brand, :model, :description, :body_type, :seats, :doors,
      :fuel_consumption_urban, :fuel_consumption_extra_urban, :fuel_type, :acceleration, :maximum_speed,
      :power, :engine_displacement, :trunk_space_min, :trunk_space_max, :fuel_tank_capacity,
      :length, :width, :height, :drivetrain, :photo, :user_id, :page, :sorting
    )
  end
end
