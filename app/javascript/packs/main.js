'use strict';

jQuery(function($) {

    /************** Toggle *********************/
    $('.toggle-id').on('click', function() {
        var id = $(this).attr('href');
        $(id).slideToggle(250);
        return false;
    });

    /************** Responsive navigation *********************/
    $('a.toggle-menu').on('click', function(){
        $('#responsive-menu').stop(true,true).slideToggle();
        return false;
    });

    /************** Search Overlay *********************/
    $('#search-icon').on('click', function() {
        $('#search-overlay').removeClass('animated bounceOutUp');
        $('#search-overlay').fadeIn(0).addClass('animated bounceInDown');
        $('.search-form-holder input[type="search"]').focus();
        return false;
    });

    $('.close-search').on('click', function() {
        $('#search-overlay').removeClass('animated bounceInDown');
        $('#search-overlay').addClass('animated bounceOutUp');
        return false;
    });

    $(document).on('keyup', function(e) {
        if (e.keyCode === 27) {
            $('#search-overlay').removeClass('animated bounceInDown');
            $('#search-overlay').addClass('animated bounceOutUp');
            return false;
        } // esc
    });

    /********************** Sorting ****************************/
    $('.search-sorting').on('change', function(e) {
        this.form.submit();
    });

    /************** Animated Hover Effects *********************/
    $('.staff-member').on('hover', function(){
        $('.overlay .social-network').addClass('animated fadeIn');
    }, function(){
        $('.overlay .social-network').removeClass('animated fadeIn');
    });

    $('.blog-thumb, .project-item').on('hover', function(){
        $('.overlay-b a').addClass('animated fadeIn');
    }, function(){
        $('.overlay-b a').removeClass('animated fadeIn');
    });

});
