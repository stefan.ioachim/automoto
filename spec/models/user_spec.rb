# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User do
  subject(:user) { described_class.new }

  describe '#cars' do
    it 'has many cars' do
      expect(user).to respond_to(:cars)
    end
  end
end
