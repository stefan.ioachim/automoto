# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Objects::Sorting do
  subject(:sorting) { described_class.new('id', 'text', 'url') }

  context 'when initialized' do
    it 'has id, text, and url attributes' do
      expect(sorting).to respond_to(:id, :text, :url)
    end

    it 'has id, text, and url attributes with values' do
      expect(sorting.id).to eq('id')
      expect(sorting.text).to eq('text')
      expect(sorting.url).to eq('url')
    end
  end
end
