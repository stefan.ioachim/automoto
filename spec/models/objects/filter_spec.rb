# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Objects::Filter do
  context 'when initialized' do
    subject(:filter) { described_class.new }

    it 'has id, text, facets, url and active attributes' do
      expect(filter).to respond_to(:id, :text, :facets, :url, :active)
    end
  end
end
