# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Car do
  subject(:car) { described_class.new }

  describe '#name' do
    let(:name) { subject.name }

    context 'when it has empty model and brand' do
      it 'returns an empty string' do
        expect(name).to eq('')
      end
    end

    context 'when it has a model and a brand' do
      it 'returns a concatenated string' do
        car.model = 'Model'
        car.brand = 'Brand'

        expect(name).to eq('Brand Model')
      end
    end
  end
end
