# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchPresenter do
  include Rails.application.routes.url_helpers

  let(:presenter) { described_class.new([]) }

  describe '#initialize' do
    it 'returns an instance' do
      expect(presenter).not_to be_nil
    end
  end

  describe '#active_params_except' do
    let(:presenter_with_params) { described_class.new(params) }
    let(:params) do
      ActionController::Parameters.new(
        {
          search: {
            brand: 'x'
          }
        }
      ).permit(search: [:brand])
    end
    let(:empty_params) { ActionController::Parameters.new.permit }

    it 'returns an empty array when presenter is not initialized with params' do
      expect(presenter.active_params_except(nil)).to eq(empty_params)
    end

    it 'returns an array when presenter is initialized with params' do
      expect(presenter_with_params.active_params_except(nil)).to eq(params[:search])
    end

    it 'returns an empty array when all received parameters are filtered' do
      expect(presenter_with_params.active_params_except(:brand)).to eq(empty_params)
    end
  end

  describe '#filters' do
    let(:filters) { presenter.filters }

    it 'returns an array  of Filter objects' do
      expect(filters).to be_kind_of(Array)
      expect(filters.first).to be_kind_of(Objects::Filter)
    end

    describe '#filters.first' do
      let(:filter) { filters.first }

      it 'has id clear' do
        expect(filter.id).to eq(:clear)
      end

      it 'first filter has text Clear All' do
        expect(filter.text).to eq('Clear All')
      end

      it 'has no facets' do
        expect(filter.facets).to be_nil
      end

      it 'has url /cars' do
        expect(filter.url).to eq(cars_path)
      end

      it 'is not active' do
        expect(filter.active).to be(false)
      end
    end

    context 'with second filter' do
      let(:filter) { filters.second }

      it 'is a Filter object' do
        expect(filter).to be_kind_of(Objects::Filter)
      end

      it 'has id seats' do
        expect(filter.id).to eq(:seats)
      end

      it 'has text By seats' do
        expect(filter.text).to eq('By seats')
      end

      it 'is not active' do
        expect(filter.active).to be(false)
      end

      describe '#facets' do
        let(:facets) { filter.facets }

        it 'returns a not empty array' do
          expect(facets).to be_kind_of(Array)
          expect(facets.length).to be > 0
        end

        it 'has the same length as Car.seats' do
          assert_equal Car.seats.length, facets.length
        end

        describe '#first' do
          let(:facet) { facets.first }

          it 'is a LinkObject type' do
            expect(facet).to be_kind_of(Objects::Link)
          end

          it 'has text 2 seats' do
            expect(facet.text).to eq('2 seats')
          end

          it 'has url with seats parameter' do
            expect(facet.url).to eq(cars_path(search: { seats: 2 }))
          end
        end
      end
    end
  end

  describe '#sortings' do
    let(:sortings) { presenter.sortings }

    it 'is a collection of Sorting objects' do
      expect(sortings).to be_kind_of(Array)
      expect(sortings.first).to be_kind_of(Objects::Sorting)
    end
  end
end
