# frozen_string_literal: true

FactoryBot.define do
  factory :car do
    brand { 'Brand' }
    sequence(:model) { |n| "model #{n}" }
    description { 'A description' }
    user
  end
end
