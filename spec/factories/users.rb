# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'password' }
  end

  factory :regular_user, class: 'User' do
    email { 'admin@server.com' }
    password { 'password' }
    password_confirmation { 'password' }
  end
end
