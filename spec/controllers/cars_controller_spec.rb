# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CarsController, type: :request do
  describe 'GET /cars' do
    context 'when it is visited' do
      it 'then it paginates the existing cars in the view' do
        FactoryBot.create_list(:car, 2)

        get '/cars'

        expect(response.body).to include('Brand model 1')
        expect(response.body).to include('Brand model 2')
      end
    end
  end

  describe 'POST /cars' do
    before do
      allow_any_instance_of(described_class).to receive(:current_user).and_return(user)
    end
    let(:user) { FactoryBot.build(:user) }

    context 'when invalid data is submitted' do
      let(:params) { { car: { brand: 'Brand', description: 'something' } } }

      it 'then a new car is not created' do
        expect { 
          post '/cars', params: params
        }.to change { Car.count }.by(0)
      end

      it 'and it renders the new template' do
        post '/cars', params: params

        created_car = user.cars.first

        expect(response).to render_template('new')
      end
    end

    context 'when valid data is submitted' do
      let(:params) { { car: { brand: 'Brand', model: 'new model', description: 'something' } } }

      it 'then it creates a new car' do
        post '/cars', params: params

        created_car = user.cars.first

        expect(created_car.brand).to eq('Brand')
        expect(created_car.model).to eq('new model')
      end

      it 'and it redirects to the car page' do
        post '/cars', params: params

        created_car = user.cars.first

        expect(response).to redirect_to("/cars/#{created_car.id}")
      end
    end
  end
end
