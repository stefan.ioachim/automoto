# frozen_string_literal: true

# Adds photo column to cars table
class AddPhotoToCars < ActiveRecord::Migration[6.1]
  def change
    add_column :cars, :photo, :string
  end
end
