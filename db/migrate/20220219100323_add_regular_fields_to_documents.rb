# frozen_string_literal: true

# Adds all the car columns to pg_search_documents table
class AddRegularFieldsToDocuments < ActiveRecord::Migration[6.1]
  def change
    add_column :pg_search_documents, :body_type, :string, { index: true }
    add_column :pg_search_documents, :seats, :integer, { index: true }
    add_column :pg_search_documents, :doors, :integer, { index: true }

    # performance specs
    add_column :pg_search_documents, :fuel_consumption_urban, :decimal, { index: true }
    add_column :pg_search_documents, :fuel_consumption_extra_urban, :decimal, { index: true }
    add_column :pg_search_documents, :fuel_type, :string, { index: true }
    add_column :pg_search_documents, :acceleration, :decimal, { index: true }
    add_column :pg_search_documents, :maximum_speed, :integer, { index: true }

    # engine specs
    add_column :pg_search_documents, :power, :integer, { index: true }
    add_column :pg_search_documents, :engine_displacement, :string, { index: true }

    # space, volume and weights
    add_column :pg_search_documents, :weight, :integer, { index: true }
    add_column :pg_search_documents, :trunk_space_min, :integer, { index: true }
    add_column :pg_search_documents, :trunk_space_max, :integer, { index: true }
    add_column :pg_search_documents, :fuel_tank_capacity, :decimal, { index: true }

    # dimensions
    add_column :pg_search_documents, :length, :integer, { index: true }
    add_column :pg_search_documents, :width, :integer, { index: true }
    add_column :pg_search_documents, :height, :integer, { index: true }

    # drivetrain
    add_column :pg_search_documents, :drivetrain, :string, { index: true }
  end
end
