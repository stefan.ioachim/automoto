# frozen_string_literal: true

# Creates the cars table
class CreateCars < ActiveRecord::Migration[6.1]
  def change
    create_table :cars do |t|
      # technical specs
      t.string :brand
      t.string :model
      t.text :description
      t.string :body_type
      t.integer :seats
      t.integer :doors

      # performance specs
      t.decimal :fuel_consumption_urban
      t.decimal :fuel_consumption_extra_urban
      t.string :fuel_type
      t.decimal :acceleration
      t.integer :maximum_speed

      # engine specs
      t.integer :power
      t.integer :engine_displacement

      # space, volume and weights
      t.integer :weight
      t.integer :trunk_space_min
      t.integer :trunk_space_max
      t.decimal :fuel_tank_capacity

      # dimensions
      t.integer :length
      t.integer :width
      t.integer :height

      # drivetrain
      t.string :drivetrain

      # indices
      t.index :brand
      t.index :model
      t.index :body_type
      t.index :seats
      t.index :doors

      t.index :fuel_consumption_urban
      t.index :fuel_consumption_extra_urban
      t.index :fuel_type
      t.index :acceleration
      t.index :maximum_speed

      t.index :power
      t.index :engine_displacement

      t.index :weight
      t.index :trunk_space_min
      t.index :trunk_space_max
      t.index :fuel_tank_capacity

      t.index :length
      t.index :width
      t.index :height

      t.index :drivetrain

      t.timestamps
    end
  end
end
