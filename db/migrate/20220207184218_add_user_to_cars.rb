# frozen_string_literal: true

# Adds a user column to cars table as a foreign key
class AddUserToCars < ActiveRecord::Migration[6.1]
  def change
    add_reference :cars, :user, foreign_key: true
  end
end
