# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_19_100323) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'cars', force: :cascade do |t|
    t.string 'brand'
    t.string 'model'
    t.text 'description'
    t.string 'body_type'
    t.integer 'seats'
    t.integer 'doors'
    t.decimal 'fuel_consumption_urban'
    t.decimal 'fuel_consumption_extra_urban'
    t.string 'fuel_type'
    t.decimal 'acceleration'
    t.integer 'maximum_speed'
    t.integer 'power'
    t.integer 'engine_displacement'
    t.integer 'weight'
    t.integer 'trunk_space_min'
    t.integer 'trunk_space_max'
    t.decimal 'fuel_tank_capacity'
    t.integer 'length'
    t.integer 'width'
    t.integer 'height'
    t.string 'drivetrain'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.string 'photo'
    t.bigint 'user_id'
    t.index ['acceleration'], name: 'index_cars_on_acceleration'
    t.index ['body_type'], name: 'index_cars_on_body_type'
    t.index ['brand'], name: 'index_cars_on_brand'
    t.index ['doors'], name: 'index_cars_on_doors'
    t.index ['drivetrain'], name: 'index_cars_on_drivetrain'
    t.index ['engine_displacement'], name: 'index_cars_on_engine_displacement'
    t.index ['fuel_consumption_extra_urban'], name: 'index_cars_on_fuel_consumption_extra_urban'
    t.index ['fuel_consumption_urban'], name: 'index_cars_on_fuel_consumption_urban'
    t.index ['fuel_tank_capacity'], name: 'index_cars_on_fuel_tank_capacity'
    t.index ['fuel_type'], name: 'index_cars_on_fuel_type'
    t.index ['height'], name: 'index_cars_on_height'
    t.index ['length'], name: 'index_cars_on_length'
    t.index ['maximum_speed'], name: 'index_cars_on_maximum_speed'
    t.index ['model'], name: 'index_cars_on_model'
    t.index ['power'], name: 'index_cars_on_power'
    t.index ['seats'], name: 'index_cars_on_seats'
    t.index ['trunk_space_max'], name: 'index_cars_on_trunk_space_max'
    t.index ['trunk_space_min'], name: 'index_cars_on_trunk_space_min'
    t.index ['user_id'], name: 'index_cars_on_user_id'
    t.index ['weight'], name: 'index_cars_on_weight'
    t.index ['width'], name: 'index_cars_on_width'
  end

  create_table 'pg_search_documents', force: :cascade do |t|
    t.text 'content'
    t.string 'searchable_type'
    t.bigint 'searchable_id'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.string 'body_type'
    t.integer 'seats'
    t.integer 'doors'
    t.decimal 'fuel_consumption_urban'
    t.decimal 'fuel_consumption_extra_urban'
    t.string 'fuel_type'
    t.decimal 'acceleration'
    t.integer 'maximum_speed'
    t.integer 'power'
    t.string 'engine_displacement'
    t.integer 'weight'
    t.integer 'trunk_space_min'
    t.integer 'trunk_space_max'
    t.decimal 'fuel_tank_capacity'
    t.integer 'length'
    t.integer 'width'
    t.integer 'height'
    t.string 'drivetrain'
    t.index  %w[searchable_type searchable_id], name: 'index_pg_search_documents_on_searchable'
  end

  create_table 'users', force: :cascade do |t|
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['email'], name: 'index_users_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true
  end

  add_foreign_key 'cars', 'users'
end
