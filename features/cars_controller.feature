Feature: Cars controller

Scenario: GET /cars
  Given The application has a list of cars
  When I visit the 'index' page
  Then I see a list of cars


Scenario: POST /cars
  Given I am a logged in user
  When I visit the 'create' page
  And I complete the car form fields

  | brand | model     | description |
  | Brand | new model | something   |

  When I submit the car form
  Then a new car is created
  And I am redirected to 'show' page
