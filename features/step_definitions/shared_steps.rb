# frozen_string_literal: true

Given('I am a logged in user') do
  @current_user = create_current_user

  visit new_user_session_path

  fill_in 'Email', with: 'admin@server.com'
  fill_in 'Password', with: 'password'
  click_button 'Log in'
end

def create_current_user
  FactoryBot.create(:regular_user)
end
