# frozen_string_literal: true

Given('The application has a list of cars') do
  FactoryBot.create_list(:car, 2)
end

When('I visit the {string} page') do |url|
  case url
  when 'index'
    visit cars_path
  when 'create'
    visit new_car_path
  end
end

When('I complete the car form fields') do |table|
  values = table.hashes.first

  fill_in 'Brand', with: values['brand']
  fill_in 'Model', with: values['model']
  fill_in 'Description', with: values['description']
end

When('I submit the car form') do
  click_button 'Create'
end

Then('a new car is created') do
  expect(@current_user.cars.length).to be >= 1
end

Then('I am redirected to {string} page') do |url|
  case url
  when 'show'
    car = @current_user.cars.first
    expect(page).to have_current_path(car_path(car.id))
  end
end

Then('I see a list of cars') do
  page.should have_content 'Brand model 1'
  page.should have_content 'Brand model 2'
end
